var smslogs = angular.module('smslogs',['ngMaterial', 'md.data.table']);

smslogs.controller('smslogsController', ['$scope', '$http', function($scope, $http){

  $http.get('app/data/smsdata.json')
    .success(function(data){
        $scope.smsData = data.documents;
    });

}]);
